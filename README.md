# Тестовое задание(Сервис уведомлений)
(Дополнительно ведётся логирование)
___

## Установка и настрйока
1. Установите Python
2. Склонируйте репозиторий
3. Установите зависимости `pip install -r requirements.txt`
4. Настройте переменные окружения(пример файла .env находится в директории fbrq). Вам нужно создать файл .env в этой же папке, указать данные для доступа к базе PostgreSQL и JWT токен
5. Примените миграции `python manage.py migrate`
6. Создайте суперпользователя для административной панели Django с помощью команды `python manage.py createsuperuser`
7. Запустите сервер `python manage.py runserver --noreload`

___
## Описание методов
### Добавление клиента(POST)
* notifications/api/addClient/
* Описание полей:
    1. phone: string - Номер телефона
    2. operator_code: string - Код оператора
    3. tag: string - Произвольный тег
    4. timezone: int - Часовой пояс в виде смещения от UTC в минутах
* Пример запроса:
```json
{
  "phone": "88005553535",
  "operator_code": "8",
  "tag": "tag",
  "timezone": 60
}
  ```
* Пример ответа:
```json
{"success": true, "entity_id": 1}
```

### Обновление клиента(POST)
* notifications/api/updateClient/
* Описание полей:
    1. id: int - id клиента
    2. phone: string - Номер телефона
    3. operator_code: string - Код оператора
    4. tag: string - Произвольный тег
    5. timezone: int - Часовой пояс в виде смещения от UTC в минутах
* Пример запроса:
```json
{
  "id": 1, 
  "phone": "88005553535",
  "operator_code": "8",
  "tag": "tag",
  "timezone": 60
}
  ```
* Пример ответа:
```json
{
  "success": true, 
  "entity_id": 1
}
```

### Удаление клиента(POST)
* notifications/api/deleteClient/
* Описание полей:
    1. id: int - id клиента
* Пример запроса:
```json
{
  "id": 1
}
  ```
* Пример ответа:
```json
{
  "success": true
}
```
___

### Добавление рассылки(POST)
* notifications/api/addMailing/
* Описание полей:
    1. start_time: string - время начала рассылки в формате Unix Timestamp_Часовой пояс в виде смещения от UTC в минутах(пример "1646661832_180")
    2. message: string - сообщение
    3. clients_filter: string - фильтр клиентов в формате JSON, доступны два поля(tag и operator_code), пример закодированной строки: `"{\"tag\":\"new client\", \"operator_code\": \"+7\"}"`. Оба поля опциональны, если фильтр не требуется, отправьте пустой JSON `"{}"`
    4. end_time: string - время окончания рассылки, формат аналогичен start_time
* Пример запроса:
```json
{
    "start_time": "1646661832_180",
    "message": "Hello World!",
    "clients_filter": "{\"tag\":\"new client\", \"operator_code\": \"+7\"}",
    "end_time": "1658772650_180"
}
```
* Пример ответа:
```json
{
    "success": true,
    "entity_id": 1
}
```

### Обновление рассылки(POST)
* notifications/api/updateMailing/
* Описание полей:
    1. id: int - id рассылки
    2. start_time: string - время начала рассылки в формате Unix Timestamp_Часовой пояс в виде смещения от UTC в минутах(пример "1646661832_180")
    3. message: string - сообщение
    4. clients_filter: string - фильтр клиентов в формате JSON, доступны два поля(tag и operator_code), пример закодированной строки: `"{\"tag\":\"new client\", \"operator_code\": \"+7\"}"`. Оба поля опциональны, если фильтр не требуется, отправьте пустой JSON `"{}"`
    5. end_time: string - время окончания рассылки, формат аналогичен start_time
* Пример запроса:
```json
{   
    "id": 1,
    "start_time": "1646661832_180",
    "message": "Hello World!",
    "clients_filter": "{\"tag\":\"new client\", \"operator_code\": \"+7\"}",
    "end_time": "1658772650_180"
}
```
* Пример ответа:
```json
{
    "success": true,
    "entity_id": 1
}
```

### Удаление рассылки(POST)
* notifications/api/deleteMailing/
* Описание полей:
    1. id: int - id рассылки
* Пример запроса:
```json
{
  "id": 1
}
  ```
* Пример ответа:
```json
{
  "success": true
}
```

### Получение общей статистики(GET)
* notifications/api/generalStats/
* Пример ответа:
```json
{
    "mailings_count": 8,
    "messages": {
        "created": 0,
        "sent": 48,
        "error": 0
    }
}
```

### Получение статистики по рассылке(GET)
* notifications/api/mailingStats/?id={id рассылки}
* Пример запроса: 
`notifications/api/mailingStats/?id=4`
* Пример ответа:
```json
{
    "entity_id": 4,
    "messages": {
        "created": 0,
        "sent": 28,
        "error": 0
    }
}
```

### Принудительный запуск рассылки(GET)
* notifications/api/processMailings/
* Пример ответа:
```json
{
    "success": true
}
```