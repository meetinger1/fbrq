import concurrent.futures
import datetime
import json
import logging
import threading
import time
from pprint import pprint

import pytz
import requests
from django.db.models import Q

from fbrq.settings import TIME_ZONE, env
from notifications.models import Mailing, Client, Message


logger = logging.getLogger(__name__)


def send_msg(mailing, client):
    """ Отправить сообщение """

    current_time = datetime.datetime.now(tz=pytz.timezone(TIME_ZONE))
    if mailing.end_time < current_time:
        logger.debug(f'Время рассылки истекло, mailing_id: {mailing.id}')
        return

    message = Message(creation_date=current_time, status=0, mailing=mailing, client=client)
    message.save()

    logger.debug(f'Сообщение создано, message_id: {message.id}')

    url = f'https://probe.fbrq.cloud/v1/send/{message.id}'

    data = {
        'id': int(message.id),
        'phone': int(client.phone),
        'text': mailing.message
    }

    headers = {
        'Authorization': f"Bearer {env.str('JWT_TOKEN')}",
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }

    response_raw = requests.post(url=url, json=data, headers=headers)

    logger.debug(f'Результат отправки сообщения, message_id: {message.id} {response_raw.content}')

    response_json = None
    try:
        response_json = response_raw.json()
    except:
        response_json = None

    if response_json is None:
        message.status = 2
    elif response_json.get('message') == 'OK':
        message.status = 1
    else:
        message.status = 2

    message.save()


def process_mailing(mailing):
    current_time = datetime.datetime.now(tz=pytz.timezone(TIME_ZONE))

    if mailing.start_time > current_time:
        logger.debug(f'Время рассылки ещё не наступило, mailing_id: {mailing.id}')
        return

    if mailing.end_time < current_time:
        logger.debug(f'Время рассылки истекло, mailing_id: {mailing.id}')
        mailing.processed = True
        mailing.save()
        return

    logger.debug(f'Запуск рассылки, mailing_id: {mailing.id}')

    clients_filter = json.loads(mailing.clients_filter)

    clients = None
    if 'operator_code' in clients_filter and 'tag' in clients_filter:
        clients = Client.objects.filter(operator_code=clients_filter['operator_code'], tag=clients_filter['tag'])
    elif 'operator_code' in clients_filter:
        clients = Client.objects.filter(operator_code=clients_filter['operator_code'])
    elif 'tag' in clients_filter:
        clients = Client.objects.filter(tag=clients_filter['tag'])
    else:
        clients = Client.objects.all()

    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for client in clients:
            futures.append(executor.submit(send_msg, mailing=mailing, client=client))

    mailing.processed = True
    mailing.save()


def process_all_mailings():
    logger.debug(f'Запуск рассылки...')
    current_time = datetime.datetime.now(tz=pytz.timezone(TIME_ZONE))

    mailings = Mailing.objects.filter(processed=False, start_time__lte=current_time, end_time__gte=current_time)

    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for mailing in mailings:
            futures.append(executor.submit(process_mailing, mailing=mailing))


class MailingWorker(threading.Thread):
    """ Поток обработки рассылок """
    def __init__(self):
        super().__init__(daemon=True)
        self.is_stopped = False
        self.is_locked = False

    def run(self) -> None:
        while not self.is_stopped:
            if not self.is_locked:
                self.is_locked = True
                process_all_mailings()
                self.is_locked = False
                time.sleep(60)
            else:
                time.sleep(1)

    def lock(self):
        logger.debug(f'MailingWorker заблокирован')
        self.is_locked = True

    def release(self):
        logger.debug(f'MailingWorker разблокирован')
        self.is_locked = False

    def stop(self):
        logger.debug(f'MailingWorker остановлен')
        self.is_stopped = True


mailing_worker = MailingWorker()
