from django.apps import AppConfig


class NotificationsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'notifications'

    def ready(self):
        from notifications.MailingWorker import mailing_worker
        print("Starting MailingWorker...")
        mailing_worker.start()
