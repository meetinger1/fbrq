class ValueIsNoneException(Exception):
    def __init__(self, key=''):
        self.key = key
        super().__init__(f'{key} is None')

    def __str__(self):
        return f'{self.key} is None'

    def get_key(self):
        return self.key
