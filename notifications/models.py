from django.db import models


# Create your models here.

class Mailing(models.Model):
    """ Модель рассылки """

    start_time = models.DateTimeField(verbose_name='Время начала рассылки')
    message = models.TextField(verbose_name='Текст сообщения')
    clients_filter = models.TextField(verbose_name='Фильтр клиентов')
    end_time = models.DateTimeField(verbose_name='Время окончания рассылки')
    processed = models.BooleanField(verbose_name='Обработано', default=False)

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return f'Mailing<{self.id} | {self.message}>'


class Client(models.Model):
    """ Модель клиента """

    phone = models.TextField(verbose_name='Номер телефона')
    operator_code = models.TextField(verbose_name='Код мобильного оператора')
    tag = models.TextField(verbose_name='Тег(произвольная метка)')
    timezone = models.IntegerField(verbose_name='Часовой пояс(от UTC)')

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return f'Client<{self.id} | {self.phone}>'


class Message(models.Model):
    """ Модель сообщения """

    status_choices = (
        (0, 'created'),
        (1, 'sent'),
        (2, 'error')
    )

    creation_date = models.DateTimeField(verbose_name='Время отправки')
    status = models.IntegerField(verbose_name='Статус отправки', choices=status_choices)

    mailing = models.ForeignKey(to='Mailing', on_delete=models.CASCADE, verbose_name='Рассылка', related_name='mailing')
    client = models.ForeignKey(to='Client', on_delete=models.CASCADE, verbose_name='Клиент', related_name='client')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return f'Message<{self.id}>'
