from django.urls import path

from notifications.views import AddClientAPIView, UpdateClientAPIView, DeleteClientAPIView, AddMailingAPIView, \
    GeneralStatsAPIView, MailingStatsAPIView, UpdateMailingAPIView, DeleteMailingAPIView, ProcessMailingsAPIView

urlpatterns = [
    path('api/addClient/', AddClientAPIView.as_view()),
    path('api/updateClient/', UpdateClientAPIView.as_view()),
    path('api/deleteClient/', DeleteClientAPIView.as_view()),
    path('api/addMailing/', AddMailingAPIView.as_view()),
    path('api/generalStats/', GeneralStatsAPIView.as_view()),
    path('api/mailingStats/', MailingStatsAPIView.as_view()),
    path('api/updateMailing/', UpdateMailingAPIView.as_view()),
    path('api/deleteMailing/', DeleteMailingAPIView.as_view()),
    path('api/processMailings/', ProcessMailingsAPIView.as_view()),
]