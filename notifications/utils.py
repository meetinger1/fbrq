import datetime

import pytz

from notifications.exceptions import ValueIsNoneException


def str_to_int(value: str):
    """ Проверка строки на возможность преобразования к целому типу """
    try:
        return int(value)
    except:
        return None


def build_dict_from_request_data(data, keys=(), check_values=False):
    """ Создание словаря с нужными ключами из данных запроса """
    res = {}
    for key in keys:
        value = data.get(key)
        if check_values and value is None:
            raise ValueIsNoneException(key)
        res[key] = value
    return res


def check_dict_values(obj):
    """ Проверка значений словаря на None """
    for key, value in obj.items():
        if value is None:
            return key
    return None


def timestamp_to_datetime(value: str):
    """ Преобразование timestamp с часовым поясом в datetime """
    # можно было использовать match-case, но ещё не все обновились до Python 3.10

    if type(value) is not str:
        return None
    split = value.split('_')

    if len(split) != 2:
        return None

    ts = str_to_int(split[0])
    tz = str_to_int(split[1])

    if ts is None:
        return None
    if tz is None:
        return None

    date = None

    try:
        date = datetime.datetime.fromtimestamp(ts, tz=pytz.FixedOffset(tz))
    except:
        date = None

    return date

