import json
import logging

from django.http import Http404
from django.shortcuts import get_object_or_404

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from notifications.MailingWorker import send_msg, process_mailing, process_all_mailings, mailing_worker
from notifications.exceptions import ValueIsNoneException
from notifications.models import Mailing, Client, Message
from notifications.utils import build_dict_from_request_data, str_to_int, timestamp_to_datetime


logger = logging.getLogger(__name__)

def build_error_json(error_msg):
    return {'success': False,
            'error': {
                'message': error_msg
            }}


class AddClientAPIView(APIView):
    """ Эндпоинт добавления нового клиента """

    def post(self, request, *args, **kwargs):
        data = request.data

        obj = None

        logger.debug(f'{type(self).__name__} | Получен новый запрос, данные запроса: {data}')
        try:
            obj = build_dict_from_request_data(data=data, keys=('phone', 'operator_code', 'tag', 'timezone'),
                                               check_values=True)
        except ValueIsNoneException as e:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, ключ {e.get_key()}')
            return Response(build_error_json(f'{e.get_key()} is None'))

        obj['timezone'] = str_to_int(obj['timezone'])

        if obj['timezone'] is None:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, некорректный часовой пояс')
            return Response(build_error_json('timezone is invalid'))

        client_obj = Client(**obj)
        client_obj.save()
        logger.debug(f'{type(self).__name__} | Клиент добавлен, client_id: {client_obj.id}')
        return Response({'success': True, 'entity_id': client_obj.id})


class UpdateClientAPIView(APIView):
    """ Эндпоинт обновления клиента """

    def post(self, request, *args, **kwargs):
        data = request.data

        logger.debug(f'{type(self).__name__} | Получен новый запрос, данные запроса: {data}')

        entity_id = data.get('id')

        if type(entity_id) is not int:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный id')
            return Response(build_error_json('id is invalid'))

        entity = None

        try:
            entity = get_object_or_404(Client, pk=entity_id)
        except Http404:
            logger.debug(f'{type(self).__name__} | Клиент не найден')
            return Response(build_error_json('entity not found'))

        if 'timezone' in data and str_to_int(data['timezone']) is None:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, некорректный часовой пояс')
            return Response(build_error_json('timezone is invalid'))

        for key in (set(data.keys()) & {'phone', 'operator_code', 'tag', 'timezone'}):
            entity.__setattr__(key, data[key])

        entity.save()
        logger.debug(f'{type(self).__name__} | Клиент обновлён, client_id: {entity_id}')
        return Response({'success': True, 'entity_id': entity_id})


class DeleteClientAPIView(APIView):
    """ Эндпоинт удаления клиента """

    def post(self, request, *args, **kwargs):
        data = request.data

        logger.debug(f'{type(self).__name__} | Получен новый запрос, данные запроса: {data}')

        entity_id = data.get('id')

        if type(entity_id) is not int:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный id')
            return Response(build_error_json('id is invalid'))

        entity = None

        try:
            logger.debug(f'{type(self).__name__} | Клиент не найден')
            entity = get_object_or_404(Client, pk=entity_id)
        except Http404:
            return Response(build_error_json('entity not found'))

        entity.delete()
        logger.debug(f'{type(self).__name__} | Клиент удалён, client_id: {entity_id}')
        return Response({'success': True})


class AddMailingAPIView(APIView):
    """ Эндпоинт добавления рассылки """

    def post(self, request, *args, **kwargs):
        data = request.data

        logger.debug(f'{type(self).__name__} | Получен новый запрос, данные запроса: {data}')

        obj = None
        try:
            obj = build_dict_from_request_data(data=data, keys=('start_time', 'message', 'clients_filter', 'end_time'),
                                               check_values=True)
        except ValueIsNoneException as e:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, ключ {e.get_key()}')
            return Response(build_error_json(f'{e.get_key()} is None'))

        obj['start_time'] = timestamp_to_datetime(obj['start_time'])
        if obj['start_time'] is None:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный start_time')
            return Response(build_error_json('start_time is invalid'))

        clients_filter = None
        try:
            clients_filter = json.loads(obj['clients_filter'])
        except:
            clients_filter = None

        if clients_filter is None:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный clients_filter')
            return Response(build_error_json('clients_filter is invalid'))

        obj['end_time'] = timestamp_to_datetime(obj['end_time'])
        if obj['end_time'] is None:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный end_time')
            return Response(build_error_json('end_time is invalid'))

        mailing = Mailing(**obj)
        mailing.save()

        logger.debug(f'{type(self).__name__} | Рассылка добавлена, mailing_id: {mailing.id}')

        if not mailing_worker.is_locked:
            mailing_worker.lock()
            try:
                process_mailing(mailing)
            except:
                mailing_worker.release()
            mailing_worker.release()

        return Response({'success': True, 'entity_id': mailing.id})


class GeneralStatsAPIView(APIView):
    """ Эндпоинт получения общей статистики """

    def get(self, request, *args, **kwargs):
        resp = {
            'mailings_count': Mailing.objects.count(),
            'messages': {
                'created': Message.objects.filter(status=0).count(),
                'sent': Message.objects.filter(status=1).count(),
                'error': Message.objects.filter(status=2).count()
            }
        }

        return Response(resp)


class MailingStatsAPIView(APIView):
    """ Эндпоинт получения статистики по конкретной рассылке """

    def get(self, request, *args, **kwargs):
        data = request.GET

        logger.debug(f'{type(self).__name__} | Получен новый запрос, данные запроса: {data}')

        mailing_id = str_to_int(data.get('id'))

        if mailing_id is None:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный id')
            return Response(build_error_json('id is invalid'))

        mailing = None

        try:
            mailing = get_object_or_404(Mailing, pk=mailing_id)
        except Http404:
            logger.debug(f'{type(self).__name__} | Рассылка не найдена')
            return Response(build_error_json('mailing not found'))

        resp = {
            'entity_id': mailing_id,
            'messages': {
                'created': Message.objects.filter(status=0, mailing=mailing).count(),
                'sent': Message.objects.filter(status=1, mailing=mailing).count(),
                'error': Message.objects.filter(status=2, mailing=mailing).count()
            }
        }

        return Response(resp)


class UpdateMailingAPIView(APIView):
    """ Эндпоинт обновления рассылки """

    def post(self, request, *args, **kwargs):
        data = request.data

        logger.debug(f'{type(self).__name__} | Получен новый запрос, данные запроса: {data}')

        entity_id = data.get('id')

        if type(entity_id) is not int:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный id')
            return Response(build_error_json('id is invalid'))

        entity = None

        try:
            entity = get_object_or_404(Mailing, pk=entity_id)
        except Http404:
            logger.debug(f'{type(self).__name__} | Рассылка не найдена')
            return Response(build_error_json('entity not found'))

        if 'start_time' in data:
            data['start_time'] = timestamp_to_datetime(data['start_time'])
            if data['start_time'] is None:
                logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный start_time')
                return Response(build_error_json('start_time is invalid'))

        if 'clients_filter' in data:
            clients_filter = None
            try:
                clients_filter = json.loads(data['clients_filter'])
            except:
                clients_filter = None

            if clients_filter is None:
                logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный clients_filter')
                return Response(build_error_json('clients_filter is invalid'))

        if 'end_time' in data:
            data['end_time'] = timestamp_to_datetime(data['end_time'])
            if data['end_time'] is None:
                logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный end_time')
                return Response(build_error_json('end_time is invalid'))

        for key in (set(data.keys()) & {'start_time', 'message', 'clients_filter', 'end_time'}):
            entity.__setattr__(key, data[key])

        entity.save()

        logger.debug(f'{type(self).__name__} | Рассылка обновлена, mailing_id: {entity_id}')

        return Response({
            'success': True,
            'entity_id': entity_id
        })


class DeleteMailingAPIView(APIView):
    """ Эндпоинт удаления рассылки """

    def post(self, request, *args, **kwargs):
        data = request.data

        logger.debug(f'{type(self).__name__} | Получен новый запрос, данные запроса: {data}')

        entity_id = data.get('id')

        if type(entity_id) is not int:
            logger.debug(f'{type(self).__name__} | Ошибка в запросе, невалидный id')
            return Response(build_error_json('id is invalid'))

        entity = None

        try:
            entity = get_object_or_404(Mailing, pk=entity_id)
        except Http404:
            logger.debug(f'{type(self).__name__} | Рассылка не найдена')
            return Response(build_error_json('entity not found'))

        entity.delete()
        logger.debug(f'{type(self).__name__} | Рассылка удалена, mailing_id: {entity_id}')

        return Response({'success': True})


class ProcessMailingsAPIView(APIView):
    """ Эндпоинт запуска обработки рассылок """
    def get(self, request, *args, **kwargs):
        if not mailing_worker.is_locked:
            mailing_worker.lock()
            try:
                process_all_mailings()
            except:
                mailing_worker.release()
            mailing_worker.release()
            return Response({'success': True})
        else:
            return Response(build_error_json('mailing already processing'))
